package simpleCRUD.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import simpleCRUD.domain.users.dao.UsersFacade;
import simpleCRUD.domain.users.model.User;
import simpleCRUD.domain.users.model.Users;

/**
 * Servlet implementation class UserCRUDServlet
 */
@WebServlet("/UserCRUD")
public class UserCRUDServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PAGE_NEXT = "/pages/users.jsp";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCRUDServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String event = request.getParameter("action");
		UsersFacade usersFacade = new UsersFacade();
	
		
		if (event != null){
			if (event.equals("addUser")) {
				addUser(request, response);
			}
		} 
		Users users = new Users();
		users.setUsers(usersFacade.users().getUsers());
		users.setLength(users.getUsers().length);
		
		request.setAttribute("users", users);
		usersFacade.closeFacade();
		// Forward
		RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher(PAGE_NEXT);
		dispatcher.forward(request, response);
	
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	private void addUser(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		User user = new User();
		boolean insertOk = false;

		if (request.getParameter("name") != null)
			user.setUserNick(request.getParameter("name"));
		if (request.getParameter("email") != null)
			user.setUserMail(request.getParameter("email"));
		if (request.getParameter("password") != null)
			user.setUserPasswd(request.getParameter("password"));

		UsersFacade usersFacade = new UsersFacade();
		insertOk = usersFacade.insertUser(user);
		usersFacade.closeFacade();

		if (insertOk) {

			request.setAttribute("addUserMsg", "Erabiltzailea ondo gorde da!!!");
		} else {

			request.setAttribute("addUserMsg", "Erabiltzailea ezin izan da gorde!!!");
		}
	}
	
	
}
