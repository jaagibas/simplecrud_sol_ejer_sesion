package simpleCRUD.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import simpleCRUD.domain.users.dao.LoginFacade;
import simpleCRUD.domain.users.model.User;

/**
 * Servlet implementation class Prueba
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		LoginFacade loginFacade = new LoginFacade();
		HttpSession session = request.getSession(true);
		String nick = request.getParameter("nick");
		String password = request.getParameter("passwd");
		String retURL = "";
		boolean exist = false;

		
		// Logic
		exist = loginFacade.exists(nick, password);
		
		if (exist == false) {

			request.setAttribute("loginMsg",
					"Erabiltzaile izena edo pasahitza gaizki daude!!");

			retURL = "/index.jsp";
		} else {
			request.setAttribute("loginMsg",
					"Erabiltzaile izena edo pasahitza ondo daude!!");
			session.setAttribute("isLoged", "true");
			retURL = "/index.jsp";
		}

		// Forward
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher(retURL);
		dispatcher.forward(request, response);
	}

}
